/**
 * ISC License (ISC)
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
import React from 'react';
import { storiesOf } from '@storybook/react';

import ResponsiveLinearLayout from '.';

import './story.css';



const layoutContrastStyles = {};

const itemContrastStyles = {
  backgroundColor: '#eee',
  border:          '1px solid gray'
};



storiesOf('ResponsiveLinearLayout', module)
  .add('Default', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('README Basic Usage Example', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-readme-layout" direction="horizontal">
        <a>Anchor 0</a>
        <a>Anchor 1</a>
        <a>Anchor 2</a>
        <div>
          <ResponsiveLinearLayout className="_rll-readme-embedded-layout" direction="vertical">
            <a>Sub Anchor 0</a>
            <a>Sub Anchor 1</a>
            <a>Sub Anchor 2</a>
          </ResponsiveLinearLayout>
        </div>
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Item Alignment (Start)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-align-items-start" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Item Alignment (End)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-align-items-end" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Item Alignment (Center)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-align-items-center" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Horizontal', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout direction="horizontal" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Horizontal (Stretch Items)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-stretch-items" direction="horiz" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Horizontal (Item Gap)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout direction="horizontal" className="_rll-item-gap" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Horizontal (Item Gap & Stretch Items)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-stretch-items _rll-item-gap" direction="horiz" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Vertical', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout direction="vertical" className="_rll-vert" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Vertical (Stretch Items)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-vert _rll-stretch-items" direction="vert" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Vertical (Item Gap)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout direction="vertical" className="_rll-vert _rll-item-gap" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ))
  .add('Vertical (Item Gap & Stretch Items)', () => (
    <ViewportNoticeWrapper>
      <ResponsiveLinearLayout className="_rll-vert _rll-stretch-items _rll-item-gap" direction="vert" style={layoutContrastStyles}>
        {_generateItems()}
      </ResponsiveLinearLayout>
    </ViewportNoticeWrapper>
  ));




function ViewportNoticeWrapper(props) {
  return (
    <div>
      <span style={{ fontFamily: 'sans', fontSize: '12px', fontStyle: 'italic', fontWeight: 'bold' }}>
        *Change the size of the window to see responsiveness of the component.
      </span>
      <br />
      <br />
      {props.children}
    </div>
  );
}


function _generateItems() {
  const items = [];

  for (let i = 0; i < 15; i++) {
    items.push(<div style={itemContrastStyles} key={i}>Item {i}</div>);
  }

  return items;
}
