# react-responsive-linear-layout [![NPM Package](https://img.shields.io/npm/v/react-responsive-linear-layout.svg?style=flat-square)][npm]

[![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)][license]

> A layout component that arranges children in a responsive single row or column using CSS
> flexbox.

At it's core, this component is basically a [CSS flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox)
with **responsiveness baked-in for you automatically when you use the provided custom CSS
properties**. As such, all CSS flexbox properties are valid when styling a
`ResponsiveLinearLayout`. The component consists of some sensible CSS defaults as well as
a few **[CSS custom properties](#css-custom-properties)** _(see the documented CSS custom
properties below for more details)_ that make styling item gaps _(I.E. margins)_ and
sizing of elements quite a bit easier.

This component is built on top of [`react-linear-layout`](https://npmjs.com/package/react-linear-layout).


[Storybook](https://bsara.gitlab.io/react-responsive-linear-layout)

[Changelog](https://gitlab.com/bsara/react-responsive-linear-layout/blob/master/CHANGELOG.md)


# Install

```bash
$ npm install --save react-responsive-linear-layout
```


# Basic Usage

[Storybook Examples](https://bsara.gitlab.io/react-responsive-linear-layout)

**MyResponsiveLinearLayoutComponent.jsx**

```jsx
import React from 'react';
import LinearLayout from 'react-linear-layout';
import ResponsiveLinearLayout from 'react-responsive-linear-layout';

import './MyResponsiveLinearLayoutComponent.css';


export default function MyResponsiveLinearLayoutComponent(props) {
  return (
    <ResponsiveLinearLayout {...props} className="layout" direction="horizontal">
      <a>Anchor 0</a>
      <a>Anchor 1</a>
      <a>Anchor 2</a>

      {/**
        * When embedding one ResponsiveLinearLayout inside another
        * (or inside a LinearLayout), an extra div is needed for
        * margins to be set correctly by both the wrapping layout
        * and the embedded layout
        */}
      <div>
        <ResponsiveLinearLayout className="embedded-layout" direction="vertical">
          <a>Sub Anchor 0</a>
          <a>Sub Anchor 1</a>
          <a>Sub Anchor 2</a>
        </ResponsiveLinearLayout>
      </div>
    </ResponsiveLinearLayout>
  );
}
```


**MyResponsiveLinearLayoutComponent.css**

```css
.layout {
  --responsive-linear-layout-item-gap: 50x;
}

.inner-layout {
  --responsive-linear-layout-item-gap: 10px;
}
```


**Output (Text Representation)**

```txt
The component above will lay out it's children similar to the following (but will also
be responsive to sizing changes):

    |----------|----------|----------|----------------|
    | Anchor 0 | Anchor 1 | Anchor 2 ||--------------||
    |          |          |          || Sub Anchor 0 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 1 ||
    |          |          |          ||--------------||
    |          |          |          || Sub Anchor 2 ||
    |          |          |          ||--------------||
    |----------|----------|----------|----------------|
```


# API

## Props

> Any prop that is acceptable by a `div` component is acceptable by a
> `ResponsvieLinearLayout` component.

- **direction** `?String` - *Default = `"horizontal"`*

  The direction to arrange the component's children.

  Possible values:
    - `"horizontal"`
    - `"horiz"`
    - `"h"`
    - `"vertical"`
    - `"vert`
    - `"v"`

- **inline** `?Boolean` - *Default = `false`*

  Determines whether or not the layout should be treated as an inline element or a block
  element.

- **ref** `?Function`

  [Forwarded ref][forwarding-refs] to underlying DOM element.


## CSS Custom Properties

- **--responsive-linear-layout-item-gap** `Same types as any "margin" CSS property`

  Sets the gap (I.E. margin) between all direct children of a `ResponsiveLinearLayout`.


## Immediate Children CSS Custom Properties

- **--responsive-linear-layout-item-size** `Same types as "width" or "height" CSS properties`

  Sets the height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.

- **--responsive-linear-layout-item-min-size** `Same types as "min-width" or "min-height" CSS properties`

  Sets the minimum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.

- **--responsive-linear-layout-item-max-size** `Same types as "max-width" or "max-height" CSS properties`

  Sets the maximum height *(if `direction` = `"vertical"`)* or width *(if `direction` =
  `"horizontal"`)* of all direct children of a `ResponsiveLinearLayout`.


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[bsara-home]: https://bsara.dev
[license]:    https://gitlab.com/bsara/react-responsive-linear-layout/blob/master/LICENSE "License"
[npm]:        https://www.npmjs.com/package/react-responsive-linear-layout                "NPM Package: react-responsive-linear-layout"

[forwarding-refs]: https://reactjs.org/docs/forwarding-refs.html "Forwarding Refs"

