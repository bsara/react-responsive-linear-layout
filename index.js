// eslint-disable-next-line spaced-comment
/*!
 * react-responsive-linear-layout v2.0.4
 *
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license
 * (https://gitlab.com/bsara/react-responsive-linear-layout/blob/master/LICENSE)
 */
import React from 'react';
import PropTypes from 'prop-types';

import { pickGlobalHtmlAttrProps } from 'pick-html-attribute-props';

import LinearLayout from 'react-linear-layout';

import './react-responsive-linear-layout.css';



function _ResponsiveLinearLayout(props, ref) {
  let classNames = 'rrll__layout';

  if (props.className) {
    classNames += ` ${props.className}`;
  }

  return React.createElement(
    LinearLayout,

    Object.assign(
      {},
      pickGlobalHtmlAttrProps(props),
      {
        ref,
        className:   classNames,
        direction:   props.direction,
        inline:      props.inline,
        omitItemGap: true
      }
    ),

    props.children
  );
}


const ResponsiveLinearLayout = React.forwardRef(_ResponsiveLinearLayout);

ResponsiveLinearLayout.propTypes = {
  direction: PropTypes.oneOf(['h', 'horiz', 'horizontal', 'v', 'vert', 'vertical']),
  inline:    PropTypes.bool
};

export default ResponsiveLinearLayout;
