/*!
 * react-responsive-linear-layout 2.0.4
 * Copyright (c) 2020, Brandon D. Sara (https://bsara.dev)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-responsive-linear-layout/blob/master/LICENSE)
 */
'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var PropTypes = _interopDefault(require('prop-types'));
var pickHtmlAttributeProps = require('pick-html-attribute-props/node');
var LinearLayout = _interopDefault(require('react-linear-layout/cjs'));

function _ResponsiveLinearLayout(props, ref) {
  let classNames = 'rrll__layout';

  if (props.className) {
    classNames += ` ${props.className}`;
  }

  return React.createElement(
    LinearLayout,

    Object.assign(
      {},
      pickHtmlAttributeProps.pickGlobalHtmlAttrProps(props),
      {
        ref,
        className:   classNames,
        direction:   props.direction,
        inline:      props.inline,
        omitItemGap: true
      }
    ),

    props.children
  );
}


const ResponsiveLinearLayout = React.forwardRef(_ResponsiveLinearLayout);

ResponsiveLinearLayout.propTypes = {
  direction: PropTypes.oneOf(['h', 'horiz', 'horizontal', 'v', 'vert', 'vertical']),
  inline:    PropTypes.bool
};

module.exports = ResponsiveLinearLayout;
