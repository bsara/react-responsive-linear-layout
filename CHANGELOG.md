# 2.0.3

- **[DEPENDENCY UPDATE]** Updated `react-linear-layout` dependency to version `2.0.2`.
- Updated dev dependencies.


# 2.0.2

- **[BUG FIX]** Fixed SSR compatibility.


# 2.0.1

- **[BREAKING CHANGE]** Updated `main` and `module` in `package.json` such that `main`
  now points to CommonJS version of library, and `module` points to ESM version.
- Upgraded to latest versions of several dev dependencies.


# 1.0.0

- Initial release _(separated from @bsara/react-layouts package)_.
